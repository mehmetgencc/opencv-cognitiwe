import cv2
import numpy as np
from flask import Flask, request

app = Flask(__name__)

image = cv2.imread('elephant.jpg')
print(image)
print(cv2.__version__)
print(np.__version__)
print(np.__version__)


@app.route("/test", methods=["GET"])
def get_test():

    message = 'OK'

    return message


@app.route("/freshness", methods=["POST"])
def process_image():

    file = request.files['image'].read()
    npimg = np.fromstring(file, np.uint8)
    # convert numpy array to image
    image = cv2.imdecode(npimg, cv2.IMREAD_COLOR)
    return str(image)


if __name__ == "__main__":
    app.run(debug=False, host='0.0.0.0', port=5000, threaded=False)
